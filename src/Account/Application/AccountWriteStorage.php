<?php

declare(strict_types=1);

namespace App\Account\Application;

use App\Account\Domain\Account;

interface AccountWriteStorage
{
    public function add(Account $account): bool;
}