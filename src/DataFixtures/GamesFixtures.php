<?php

namespace App\DataFixtures;

use App\Account\Domain\Account;
use App\Game\Domain\Game;
use App\Game\Domain\Module;
use App\Game\Domain\Participant;
use App\Game\Domain\ParticipantState;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class GamesFixtures extends Fixture implements FixtureGroupInterface, DependentFixtureInterface
{
    public const MODULE_REFERENCE_PREFIX = 'module_';

    public static function getGroups(): array
    {
        return ['base'];
    }

    public function getDependencies()
    {
        return array(
            AuthorizationFixtures::class,
            AccountFixtures::class,
            ModulesFixtures::class
        );
    }

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        // create games
        for ($i = 0; $i < 20; $i++) {
            $moduleI = rand(0, 1);
            /** @var Module $module */
            $module = $this->getReference(ModulesFixtures::MODULE_REFERENCE_PREFIX . $moduleI);
            /** @var Account $account */
            $account = $this->getReference(AccountFixtures::ACCOUNT_REFERENCE_PREFIX . $i);

            $game = new Game();
            $game->setModule($module);
            $game->setTag($module->getTag() . ($i + 1));
            $game->setTitle('Dummy Game ' . ($i + 1));
            $game->setMasterId($account->getId());
            $game->setTurn(0);
            $game->setCreatedAt((new \DateTimeImmutable())->getTimestamp());

            if ($i < 4) {
                $game->setStatus(Game::STATE_OPEN_GAME);
            } elseif ($i < 6) {
                $game->setStatus(Game::STATE_REGISTRATION_IS_OVER);
            } elseif ($i < 10) {
                $game->setStatus(Game::STATE_ACTIVE);
                $game->setStartedAt((new \DateTimeImmutable())->getTimestamp());
            } elseif ($i < 15) {
                $game->setStatus(Game::STATE_FINISHED);
                $game->setStartedAt((new \DateTimeImmutable())->getTimestamp());
                $game->setEndedAt((new \DateTimeImmutable())->getTimestamp());
            } elseif ($i < 18) {
                $game->setStatus(Game::STATE_CANCELLED);
                $game->setStartedAt((new \DateTimeImmutable())->getTimestamp());
                $game->setEndedAt((new \DateTimeImmutable())->getTimestamp());
            } else {
                $game->setStatus(Game::STATE_DELETED);
                $game->setStartedAt((new \DateTimeImmutable())->getTimestamp());
                $game->setEndedAt((new \DateTimeImmutable())->getTimestamp());
            }

            $manager->persist($game);

            // add players to games
            if ($game->isOpen()) {
                /** @var Account $participantUser */
                $adminUser = $this->getReference(AccountFixtures::ACCOUNT_REFERENCE_PREFIX . '0');
                $state = ($i == 0 || rand(0, 9) < 5) ? ParticipantState::claimer() : ParticipantState::player();
                $participant = Participant::create($game->getId(), $adminUser->getId(), $state);

                $manager->persist($participant);

                for ($j = 0; $j < 4; $j++) {
                    /** @var Account $participantUser */
                    $participantUser = $this->getReference(AccountFixtures::ACCOUNT_REFERENCE_PREFIX . ($i + $j + 1));
                    $state = $j < 2 ? ParticipantState::claimer() : ParticipantState::player();
                    $participant = Participant::create($game->getId(), $participantUser->getId(), $state);

                    $manager->persist($participant);
                }
            } else {
                if (rand(0, 9) > 8) {
                    /** @var Account $participantUser */
                    $adminUser = $this->getReference(AccountFixtures::ACCOUNT_REFERENCE_PREFIX . '0');
                    $participant = Participant::create($game->getId(), $adminUser->getId(), ParticipantState::player());

                    $manager->persist($participant);
                }

                for ($j = 0; $j < 3; $j++) {
                    /** @var Account $participantUser */
                    $participantUser = $this->getReference(AccountFixtures::ACCOUNT_REFERENCE_PREFIX . ($i + $j + 1));
                    $participant = Participant::create($game->getId(), $participantUser->getId(), ParticipantState::player());

                    $manager->persist($participant);
                }
            }
        }

        $manager->flush();
    }
}