<?php

declare(strict_types=1);

namespace App\Game\Domain;

use LitGroup\Enumerable\Enumerable;

final class ParticipantState extends Enumerable
{
    private const ROLE_CLAIMER = 10;
    private const ROLE_PLAYER = 20;
    private const ROLE_BANNED = 99;

    public static function claimer(): self
    {
        return self::createEnum(self::ROLE_CLAIMER);
    }

    public static function player(): self
    {
        return self::createEnum(self::ROLE_PLAYER);
    }

    public static function banned(): self
    {
        return self::createEnum(self::ROLE_BANNED);
    }
}