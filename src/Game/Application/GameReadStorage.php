<?php

declare(strict_types=1);

namespace App\Game\Application;

use App\Game\Domain\Game;
use Ramsey\Uuid\UuidInterface;

interface GameReadStorage
{
    /**
     * @param UuidInterface $id
     * @return Game[]
     */
    public function getGamesByMasterId(UuidInterface $id): array;

    /**
     * @param UuidInterface $id
     * @return Game[]
     */
    public function getGamesClaimingById(UuidInterface $id): array;

    /**
     * @param UuidInterface $id
     * @return Game[]
     */
    public function getGamesPlayingById(UuidInterface $id): array;

    /**
     * @param UuidInterface $id
     * @return Game[]
     */
    public function getGamesOpenToUserById(UuidInterface $id): array;
}