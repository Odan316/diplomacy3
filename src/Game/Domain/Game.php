<?php

declare(strict_types=1);

namespace App\Game\Domain;

use App\Account\Domain\Account;
use Ramsey\Uuid\UuidInterface;

class Game
{
    const STATE_OPEN_GAME = 1;
    const STATE_REGISTRATION_IS_OVER = 2;
    const STATE_ACTIVE = 3;
    const STATE_FINISHED = 10;
    const STATE_CANCELLED = 11;
    const STATE_DELETED = 90;

    private UuidInterface $id;

    private string $title;

    private string $tag;

    private int $status;

    private int $createdAt;

    private int $startedAt;

    private int $endedAt;

    private int $turn;

    //private Account $master;

    private Module $module;

    //private GameUser $participantsRoles;
    /**
     * @var UuidInterface
     */
    private UuidInterface $masterId;

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTag(): string
    {
        return $this->tag;
    }

    /**
     * @param string $tag
     */
    public function setTag(string $tag): void
    {
        $this->tag = $tag;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getCreatedAt(): int
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt(int $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getStartedAt(): int
    {
        return $this->startedAt;
    }

    /**
     * @param int $startedAt
     */
    public function setStartedAt(int $startedAt): void
    {
        $this->startedAt = $startedAt;
    }

    /**
     * @return int
     */
    public function getEndedAt(): int
    {
        return $this->endedAt;
    }

    /**
     * @param int $endedAt
     */
    public function setEndedAt(int $endedAt): void
    {
        $this->endedAt = $endedAt;
    }

    /**
     * @return int
     */
    public function getTurn(): int
    {
        return $this->turn;
    }

    /**
     * @param int $turn
     */
    public function setTurn(int $turn): void
    {
        $this->turn = $turn;
    }

    /**
     * @return Account
     */
    public function getMaster(): Account
    {
        return $this->master;
    }

    /**
     * @param Account $master
     */
    public function setMaster(Account $master): void
    {
        $this->master = $master;
    }

    /**
     * @return Module
     */
    public function getModule(): Module
    {
        return $this->module;
    }

    /**
     * @param Module $module
     */
    public function setModule(Module $module): void
    {
        $this->module = $module;
    }

    //** @var User[]  */
    //private array $participants;
    public function setMasterId(UuidInterface $id)
    {
        $this->masterId = $id;
    }

    public function isOpen(): bool
    {
        return $this->status === self::STATE_OPEN_GAME;
    }

    public function isActive(): bool
    {
        return $this->status === self::STATE_ACTIVE;
    }
}