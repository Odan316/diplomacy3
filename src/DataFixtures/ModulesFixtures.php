<?php

namespace App\DataFixtures;

use App\Game\Domain\Module;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ModulesFixtures extends Fixture implements FixtureGroupInterface
{
    public const MODULE_REFERENCE_PREFIX = 'module_';

    public static function getGroups(): array
    {
        return ['base'];
    }

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {

        $module = new Module();
        $module->setTitle('Vestria');
        $module->setActive(true);
        $module->setAuthor('Ownag');
        $module->setTag('VES');
        $manager->persist($module);

        $this->addReference(self::MODULE_REFERENCE_PREFIX.'0', $module);

        $module = new Module();
        $module->setTitle('Holy Roman Empire');
        $module->setActive(true);
        $module->setAuthor('Odan');
        $module->setTag('HRE');
        $manager->persist($module);

        $this->addReference(self::MODULE_REFERENCE_PREFIX.'1', $module);

        $manager->flush();
    }
}