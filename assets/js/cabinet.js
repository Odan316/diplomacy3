const $ = require('jquery');

$(function(){
    $('.create_game').on('click', function(){
        $("#cabinet_game_creation").modal();
    });

    $(document).on('click', '.make_claim, .accept_claim, .refuse_claim', function(){
        let url = $(this).data('url');
        $.ajax({
            type: "POST",
            async: false,
            url: url,
            data: {
                _csrf : $('meta[name="csrf-token"]').attr("content")
            },
            success: function(data){
                location.reload();
            }
        });
    });

    $(document).on('click', '.start_game', function(){
        var gameId = $('#b_game_info').data('game-id');
        $.ajax({
            type: "POST",
            async: false,
            url: "/cabinet/start-game?gameId=" + gameId,
            data: {
                _csrf : $('meta[name="csrf-token"]').attr("content")
            },
            success: function(data){
                $('#cabinet_game_info').html(data);
            }
        });
    });

    $(document).on('click', '.cancel_game', function(){
        var gameId = $('#b_game_info').data('game-id');
        $.ajax({
            type: "POST",
            async: false,
            url: "/cabinet/cancel-game?gameId=" + gameId,
            data: {
                _csrf : $('meta[name="csrf-token"]').attr("content")
            },
            success: function(data){
                $('#gm_list').children('[data-game-id='+gameId+']').detach();
                $('#cabinet_game_info').html(data);
            }
        });
    });

    $(document).on('click', '.end_game', function(){
        var gameId = $('#b_game_info').data('game-id');
        $.ajax({
            type: "POST",
            async: false,
            url: "/cabinet/end-game?gameId=" + gameId,
            data: {
                _csrf : $('meta[name="csrf-token"]').attr("content")
            },
            success: function(data){
                $('#gm_list').children('[data-game-id='+gameId+']').detach();
                $('#cabinet_game_info').html(data);
            }
        });
    });
});