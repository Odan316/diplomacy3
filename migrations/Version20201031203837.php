<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201031203837 extends AbstractMigration
{
    public function getDescription() : string
    {
        return "Adding 'user' table";
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            "Migration can only be executed safely on 'mysql'.");

        $this->addSql('CREATE TABLE user 
                (
                    id BINARY(16) NOT NULL,
                    username VARCHAR(180) NOT NULL,
                    password VARCHAR(255) NOT NULL, 
                    roles JSON NOT NULL,  
                    PRIMARY KEY(id),
                    UNIQUE INDEX UQ_user_username (username)
                ) 
                DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB'
        );
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            "Migration can only be executed safely on 'mysql'.");

        $this->addSql('DROP TABLE user');
    }
}
