<?php

declare(strict_types=1);

namespace App\PersonalCabinet\Application;

final class Account
{
    private string $displayName;

    public function __construct(string $displayName)
    {
        $this->displayName = $displayName;
    }

    public function getDisplayName(): string
    {
        return $this->displayName;
    }
}