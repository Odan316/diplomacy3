<?php

declare(strict_types=1);

namespace App\Account\Presentation\ValidationEntities;

use Symfony\Component\Validator\Constraints as Assert;

final class SignupRequest
{
    /**
     * @Assert\NotBlank()
     * @Assert\Length(min="3", max="12")
     */
    public string $username;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min="3", max="64")
     */
    public string $password;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min="3", max="24")
     */
    public string $displayName;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min="3", max="64")
     * @Assert\Email()
     */
    public string $email;

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getDisplayName(): string
    {
        return $this->displayName;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }
}