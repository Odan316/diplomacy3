<?php

declare(strict_types=1);

namespace App\PersonalCabinet\Application;

use Ramsey\Uuid\UuidInterface;

interface User
{
    public function getId(): UuidInterface;
}