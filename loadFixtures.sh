#!/usr/bin/env bash

printf "Drop DB \r\n"
php bin/console doctrine:database:drop --force
printf "Create DB \r\n"
php bin/console doctrine:database:create
printf "Apply migrations \r\n"
php bin/console doctrine:migration:migrate --no-interaction
printf "Load fixtures \r\n"
php bin/console doctrine:fixtures:load --no-interaction