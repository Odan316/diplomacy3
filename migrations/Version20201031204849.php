<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201031204849 extends AbstractMigration
{
    public function getDescription() : string
    {
        return "Adding 'module' and 'game' table";
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            "Migration can only be executed safely on 'mysql'.");

        $this->addSql('CREATE TABLE module 
                (
                    id BINARY(16) NOT NULL,
                    title VARCHAR(36) NOT NULL, 
                    tag VARCHAR(6) NOT NULL, 
                    author VARCHAR(64) NOT NULL, 
                    active TINYINT(1) NOT NULL, 
                    PRIMARY KEY(id)
                ) 
                DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB'
        );
        $this->addSql('CREATE TABLE game 
                (
                    id BINARY(16) NOT NULL,
                    master_id BINARY(16) NULL, 
                    module_id BINARY(16) NULL, 
                    title VARCHAR(64) NOT NULL, 
                    tag VARCHAR(32) NOT NULL, 
                    status TINYINT NOT NULL, 
                    created_at INT NOT NULL, 
                    started_at INT, 
                    ended_at INT, 
                    turn TINYINT NOT NULL,
                    UNIQUE INDEX UQ_game_tag (tag),
                    INDEX IX_game_master_id (master_id), 
                    INDEX IX_game_module_id (module_id), 
                    PRIMARY KEY(id)
                ) 
                DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB'
        );
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            "Migration can only be executed safely on 'mysql'.");

        $this->addSql('DROP TABLE game');
        $this->addSql('DROP TABLE module');
    }
}
