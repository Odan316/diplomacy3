<?php

namespace App\DataFixtures;

use App\Authorization\Infrastructure\Entity\User;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Faker\Factory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AuthorizationFixtures extends Fixture implements FixtureGroupInterface
{
    public const USER_REFERENCE_PREFIX = 'user_';

    private UserPasswordEncoderInterface $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public static function getGroups(): array
    {
        return ['base'];
    }

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        for ($i = 0; $i < 30; $i++) {
            if ($i === 0) {
                $username = 'admin';
            } else {
                $username = $faker->userName;
            }

            $user = User::createNew($username);
            $user->setPassword($this->encoder->encodePassword($user, '111111'));
            $manager->persist($user);

            $this->addReference(self::USER_REFERENCE_PREFIX.$i, $user);
        }

        $manager->flush();
    }
}