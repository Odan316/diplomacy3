<?php

declare(strict_types=1);

namespace App\Common\Presentation\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomepageController extends AbstractController
{
    /**
     * @return Response
     */
    public function index()
    {
        if (empty($this->getUser())) {
            return $this->redirectToRoute('app_login');
        } else {
            return $this->redirectToRoute('cabinet');
        }
    }
}