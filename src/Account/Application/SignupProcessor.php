<?php

declare(strict_types=1);

namespace App\Account\Application;

use App\Account\Domain\Account;
use App\Account\Domain\Events\AccountRegistered;
use App\Account\Presentation\ValidationEntities\SignupRequest;
use SimpleBus\SymfonyBridge\Bus\EventBus;

final class SignupProcessor
{
    private AccountWriteStorage $accountWriteStorage;

    private EventBus $eventBus;

    public function __construct(AccountWriteStorage $accountWriteStorage, EventBus $eventBus)
    {
        $this->accountWriteStorage = $accountWriteStorage;
        $this->eventBus = $eventBus;
    }

    public function registerAccount(SignupRequest $signupRequest)
    {
        $account = Account::create();
        $account->setEmail($signupRequest->getEmail());
        $account->setDisplayName($signupRequest->getDisplayName());

        $this->accountWriteStorage->add($account);

        $this->eventBus->handle(new AccountRegistered($account->getId(), $signupRequest->getUsername(), $signupRequest->getPassword()));
    }
}