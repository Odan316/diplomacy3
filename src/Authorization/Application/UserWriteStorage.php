<?php

declare(strict_types=1);

namespace App\Authorization\Application;

use App\Authorization\Infrastructure\Entity\User;

interface UserWriteStorage
{
    public function add(User $user): bool;

}