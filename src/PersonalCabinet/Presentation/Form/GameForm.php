<?php

declare(strict_types=1);

namespace App\PersonalCabinet\Presentation\Form;

use App\PersonalCabinet\Domain\Module;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class GameForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // proper form
        $builder
            ->add('title', TextType::class, [
                'required' => true
            ])
            ->add('moduleTag', EntityType::class, [
                'class'        => Module::class,
                'choice_label' => 'title',
                'choice_value' => 'tag',
                'label'        => 'Select module'
            ])
            ->add('create', SubmitType::class, [
            ]);
    }
}