all: build-images app

build-images:
	docker compose build

app:
	docker compose -p diplo down; \
	docker compose -p diplo up -d webapp webserver

stop:
	docker compose -p diplo down;

yarn:
	docker exec -it diplo yarn install;

yarn:
	docker exec -it diplo yarn install;
