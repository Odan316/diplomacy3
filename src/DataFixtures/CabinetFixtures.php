<?php

namespace App\DataFixtures;

use App\Account\Domain\Account;
use App\Authorization\Infrastructure\Entity\User;
use App\PersonalCabinet\Domain\Game;
use App\PersonalCabinet\Domain\Module;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Faker\Factory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class CabinetFixtures extends Fixture implements FixtureGroupInterface
{
    private UserPasswordEncoderInterface $encoder;

    public static function getGroups(): array
    {
        return [
            'cabinet'
        ];
    }

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        $accounts = [];
        for ($i = 0; $i < 30; $i++) {
            $user = new User();
            if ($i === 0) {
                $user->setUsername('admin');
            } else {
                $user->setUsername($faker->userName);
            }

            $user->setPassword($this->encoder->encodePassword($user, '111111'));
            $manager->persist($user);

            $account = new Account();
            $account->setId($user->getId());
            $account->setEmail($faker->email);
            $account->setDisplayName($faker->name);

            $manager->persist($account);

            $accounts[] = $account;

        }

        // create dummy game module
        $module = new Module();
        $module->setTitle('Dummy Module');
        $module->setTag('DUM');
        $module->setAuthor('Jon Doe');
        $module->setActive(true);
        $manager->persist($module);

        // create games
        for ($i = 0; $i < 10; $i++) {

            $game = new Game();
            $game->setModule($module);
            $game->setTag($module->getTag() . ($i + 1));
            $game->setTitle('Dummy Game ' . ($i + 1));
            $game->setMaster($accounts[$i]);
            $game->setTurn(0);
            $game->setCreatedAt((new \DateTimeImmutable())->getTimestamp());

            if ($i < 2) {
                $game->setStatus(Game::STATE_OPEN_GAME);
            } elseif ($i < 3) {
                $game->setStatus(Game::STATE_REGISTRATION_IS_OVER);
            } elseif ($i < 6) {
                $game->setStatus(Game::STATE_ACTIVE);
                $game->setStartedAt((new \DateTimeImmutable())->getTimestamp());
            } elseif ($i < 7) {
                $game->setStatus(Game::STATE_FINISHED);
                $game->setStartedAt((new \DateTimeImmutable())->getTimestamp());
                $game->setEndedAt((new \DateTimeImmutable())->getTimestamp());
            } elseif ($i < 8) {
                $game->setStatus(Game::STATE_CANCELLED);
                $game->setStartedAt((new \DateTimeImmutable())->getTimestamp());
                $game->setEndedAt((new \DateTimeImmutable())->getTimestamp());
            } elseif ($i < 10) {
                $game->setStatus(Game::STATE_DELETED);
                $game->setStartedAt((new \DateTimeImmutable())->getTimestamp());
                $game->setEndedAt((new \DateTimeImmutable())->getTimestamp());
            }

            $manager->persist($game);

            // add players to games
            /*if($game->isOpen()){
                for($j = 0; $j < 4; $j++){
                    $gameRole = new GameUser();
                    $gameRole->setGame($game);
                    $gameRole->setUser($users[rand($i+$j+1, 29)]);
                    if($j < 2){
                        $gameRole->setRole(GameUser::ROLE_CLAIMER);
                    } else {
                        $gameRole->setRole(GameUser::ROLE_PLAYER);
                    }

                    $manager->persist($gameRole);
                }
            } else {
                for($j = 0; $j < 3; $j++){
                    $gameRole = new GameUser();
                    $gameRole->setGame($game);
                    $gameRole->setUser($users[rand($i+$j+1, 29)]);
                    $gameRole->setRole(GameUser::ROLE_PLAYER);

                    $manager->persist($gameRole);
                }
            }*/
        }

        $manager->flush();
    }
}