<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210206083047 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Adding \'game_participant\' table';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE TABLE game_participant
                (
                    game_id BINARY(16) NOT NULL,
                    user_id BINARY(16) NOT NULL,
                    state SMALLINT NOT NULL,
                    PRIMARY KEY(game_id, user_id)
                ) 
                DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB'
        );
    }

    public function down(Schema $schema) : void
    {

        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            "Migration can only be executed safely on 'mysql'.");

        $this->addSql('DROP TABLE game_participant');
    }
}
