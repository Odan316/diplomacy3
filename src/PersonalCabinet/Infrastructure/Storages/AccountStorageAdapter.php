<?php

declare(strict_types=1);

namespace App\PersonalCabinet\Infrastructure\Storages;

use App\PersonalCabinet\Application\Account;
use App\PersonalCabinet\Application\AccountReadStorage;
use App\Account\Application\AccountReadStorage as AccountStorageOuter;
use Ramsey\Uuid\UuidInterface;

final class AccountStorageAdapter implements AccountReadStorage
{
    private AccountStorageOuter $outerStorage;

    public function __construct(AccountStorageOuter $outerStorage)
    {
        $this->outerStorage = $outerStorage;
    }

    public function get(UuidInterface $id, $lockMode = null, $lockVersion = null): ?Account
    {
        $outerAccount = $this->outerStorage->get($id);
        if(null === $outerAccount){
            return null;
        }

        return new Account($outerAccount->getDisplayName());
    }
}