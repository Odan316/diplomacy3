<?php

declare(strict_types=1);

namespace App\PersonalCabinet\Application;

use Ramsey\Uuid\UuidInterface;

interface AccountReadStorage
{
    public function get(UuidInterface $id, $lockMode = null, $lockVersion = null): ?Account;

}