<?php

declare(strict_types=1);

namespace App\PersonalCabinet\Presentation\Controller;

use App\PersonalCabinet\Application\AccountReadStorage;
use App\PersonalCabinet\Application\User;
use App\PersonalCabinet\Application\GameReadStorage;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class CabinetController extends AbstractController
{
    /**
     * @param Request $request
     * @param AccountReadStorage $accountStorage
     * @param GameReadStorage $gamesStorage
     * @return RedirectResponse|Response
     */
    public function index(Request $request, AccountReadStorage $accountStorage, GameReadStorage $gamesStorage)
    {
        /** @var User $user */
        $user = $this->getUser();

        $account = $accountStorage->get($user->getId());
        if(null === $account){
            throw new NotFoundHttpException();
        }
        // $gameForm = $this->createForm(GameForm::class);

        return $this->render('cabinet/index.html.twig', [
            'account' => $account,
            'masterGames' => $gamesStorage->getGamesByMasterId($user->getId()),
            'claimingGames' => $gamesStorage->getGamesClaimingById($user->getId()),
            'playingGames' => $gamesStorage->getGamesPlayingById($user->getId()),
            'openGames'  => $gamesStorage->getGamesOpenToUserById($user->getId()),
            'modules' => [],
            'game' => null,
            'userRole' => null,
            //'gameForm'     => $gameForm->createView()
        ]);
    }


    /**
     * @Route("/cabinet/reggo", name="cabinet1")
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws Exception
     */
    public function index1(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $account = new Account($user->getUsername(), $user->getEmail(), $user->getDisplayName());
        $modules = [];

        $gameFormModel = new GameCreateRequest();
        $gameForm = $this->createForm(GameForm::class, $gameFormModel);

        //$gameRepo = $this->getDoctrine()->getRepository(Game::class);
        //$gameUserRepo = $this->getDoctrine()->getRepository(GameUser::class);

        /*

        $gameForm->handleRequest($request);
        if ($gameForm->isSubmitted() && $gameForm->isValid()) {

            $sameModuleGamesCount = $gameRepo->count(['module' => $gameForForm->getModule()]);
            $gameForForm->setTag($gameForForm->getModule()->getTag().($sameModuleGamesCount+1));
            $gameForForm->setCreatedAt(new DateTime());
            $gameForForm->setTurn(0);
            $gameForForm->setMaster($user);
            $gameForForm->setStatus(Game::STATE_OPEN_GAME);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($gameForForm);
            $entityManager->flush();

            return $this->redirectToRoute('cabinet', ['gameId' => $gameForForm->getId()]);
        }*/

        return $this->render('cabinet/index.html.twig', [
            'account'         => $account,
            'masterGames' => [],
            'claimerGames' => [],
            'playerGames'  => [],
            'openGames'    => [],
            'game'         => null,
            'userRole'     => null,
            'gameForm'     => $gameForm->createView()
        ]);
    }

    /**
     * @Route("/cabinet/make-claim", name="cabinet/claim")
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws Exception
     */
    public function makeClaim(Request $request)
    {
        $result = $this->changeParticipantRole(
            $request->get('gameId', 0),
            $request->get('userId', 0),
            GameUser::ROLE_CLAIMER);

        return $this->json(['result' => $result]);
    }

    /**
     * @Route("/cabinet/accept-claim", name="cabinet/accept")
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws Exception
     */
    public function acceptClaim(Request $request)
    {
        $result = $this->changeParticipantRole(
            $request->get('gameId', 0),
            $request->get('userId', 0),
            GameUser::ROLE_PLAYER);

        return $this->json(['result' => $result]);
    }

    /**
     * @Route("/cabinet/refuse-claim", name="cabinet/refuse")
     * @param Request $request
     * @return RedirectResponse|Response
     * @throws Exception
     */
    public function refuseClaim(Request $request)
    {
        $result = $this->changeParticipantRole(
            $request->get('gameId', 0),
            $request->get('userId', 0),
            GameUser::ROLE_BANNED);

        return $this->json(['result' => $result]);
    }

    /**
     * @param int $gameId
     * @param int $userId
     * @param int $newRole
     * @return bool
     */
    protected function changeParticipantRole(int $gameId, int $userId, int $newRole)
    {
        /** @var UserRepository $userRepo */
        $userRepo = $this->getDoctrine()->getRepository(User::class);
        $user = $userRepo->find($userId);

        /** @var GameRepository $gameRepo */
        $gameRepo = $this->getDoctrine()->getRepository(Game::class);
        $game = $gameRepo->find($gameId);

        /** @var GameUserRepository $gameUserRepo */
        $gameUserRepo = $this->getDoctrine()->getRepository(GameUser::class);
        $gameRole = $gameUserRepo->findOneBy(['user' => $user, 'game' => $game]);

        if(!empty($gameRole)){
            $gameRole->setRole($newRole);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($gameRole);
            $entityManager->flush();

            $result = true;
        } else {
            $result = false;
        }

        return $result;
    }
}
