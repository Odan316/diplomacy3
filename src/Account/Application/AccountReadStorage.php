<?php

declare(strict_types=1);

namespace App\Account\Application;

use App\Account\Domain\Account;
use Ramsey\Uuid\UuidInterface;

interface AccountReadStorage
{
    public function get(UuidInterface $id, $lockMode = null, $lockVersion = null): ?Account;

}