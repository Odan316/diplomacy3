<?php

declare(strict_types=1);

namespace App\Game\Domain;

use Ramsey\Uuid\UuidInterface;

class Participant
{
    private UuidInterface $gameId;

    private UuidInterface $userId;

    private int $state;

    public static function create(UuidInterface $gameId, UuidInterface $userId, ParticipantState $state): Participant
    {
        $participant = new Participant();
        $participant->gameId = $gameId;
        $participant->userId = $userId;
        $participant->state = $state->getRawValue();

        return $participant;
    }

    /**
     * @return UuidInterface
     */
    public function getGameId(): UuidInterface
    {
        return $this->gameId;
    }

    /**
     * @return UuidInterface
     */
    public function getUserId(): UuidInterface
    {
        return $this->userId;
    }

    /**
     * @return ParticipantState
     */
    public function getState(): ParticipantState
    {
        return ParticipantState::getValueOf($this->state);
    }

}