<?php

declare(strict_types=1);

namespace App\Account\Presentation\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class SignupForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, [
                'label' => 'Login',
            ])
            ->add('password', PasswordType::class, [
                'label' => 'Password',
            ])
            ->add('displayName', TextType::class, [
                'label' => 'Name for displaying',
            ])
            ->add('email', EmailType::class, [
                'required' => true,
            ])
            ->add('sign_up', SubmitType::class)
        ;
    }
}