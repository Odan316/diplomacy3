<?php

namespace App\DataFixtures;

use App\Account\Domain\Account;
use App\Authorization\Infrastructure\Entity\User;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Faker\Factory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AccountFixtures extends Fixture implements FixtureGroupInterface, DependentFixtureInterface
{
    public const ACCOUNT_REFERENCE_PREFIX = 'account_';

    public static function getGroups(): array
    {
        return ['base'];
    }

    public function getDependencies(): array
    {
        return array(
            AuthorizationFixtures::class,
        );
    }

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        for ($i = 0; $i < 30; $i++) {
            /** @var User $user */
            $user = $this->getReference(AuthorizationFixtures::USER_REFERENCE_PREFIX.$i);

            $account = new Account();
            $account->setId($user->getId());
            $account->setEmail($faker->email);
            $account->setDisplayName($faker->name);

            $manager->persist($account);

            $this->addReference(self::ACCOUNT_REFERENCE_PREFIX.$i, $account);
        }

        $manager->flush();
    }
}