<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201031204744 extends AbstractMigration
{
    public function getDescription() : string
    {
        return "Adding 'account' table";
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            "Migration can only be executed safely on 'mysql'.");

        $this->addSql('CREATE TABLE account 
                (
                    id BINARY(16) NOT NULL,
                    email VARCHAR(255) NOT NULL, 
                    display_name VARCHAR(255) NOT NULL, 
                    PRIMARY KEY(id)
                ) 
                DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB'
        );
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            "Migration can only be executed safely on 'mysql'.");

        $this->addSql('DROP TABLE account');
    }
}
