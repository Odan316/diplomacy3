<?php

declare(strict_types=1);

namespace App\Account\Domain\Events;

use App\Common\Application\DomainEvent;
use Ramsey\Uuid\UuidInterface;
use SimpleBus\Message\Name\NamedMessage;

final class AccountRegistered implements DomainEvent, NamedMessage
{
    private UuidInterface $userId;
    private string $username;
    private string $password;

    public function __construct(UuidInterface $userId, string $username, string $password)
    {
        $this->userId = $userId;
        $this->username = $username;
        $this->password = $password;
    }

    public static function messageName(): string
    {
        return 'account_registered';
    }

    public function getUserId(): UuidInterface
    {
        return $this->userId;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getPassword(): string
    {
        return $this->password;
    }
}