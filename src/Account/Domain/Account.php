<?php

declare(strict_types=1);

namespace App\Account\Domain;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class Account
{
    private UuidInterface $id;

    private string $email;

    private string $displayName;

    public static function create(): Account
    {
        $account = new self;

        $account->setId(Uuid::uuid4());

        return $account;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getDisplayName(): string
    {
        return $this->displayName;
    }

    public function setId(UuidInterface $id): void
    {
        $this->id = $id;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function setDisplayName(string $displayName): void
    {
        $this->displayName = $displayName;
    }
}