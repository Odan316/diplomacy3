<?php

declare(strict_types=1);

namespace App\Game\Infrastructure\Persistence\Doctrine;

use App\Game\Application\GameReadStorage;
use App\Game\Domain\Game;
use App\Game\Domain\Participant;
use App\Game\Domain\ParticipantState;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;
use Ramsey\Uuid\UuidInterface;

/**
 * @method Game|null find($id, $lockMode = null, $lockVersion = null)
 * @method Game|null findOneBy(array $criteria, array $orderBy = null)
 * @method Game[]    findAll()
 * @method Game[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
final class GameRepository extends ServiceEntityRepository implements GameReadStorage
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Game::class);
    }

    public function get(string $id): ?Game
    {
        return $this->find($id);
    }

    /**
     * @inheritDoc
     */
    public function getGamesByMasterId(UuidInterface $id): array
    {
        return $this->findBy(['masterId' => $id]);
    }

    public function getGamesByParticipant(UuidInterface $id, int $state = null): array
    {
        $qb = $this->createQueryBuilder('g');
        $qb
            ->leftJoin(Participant::class, 'p', Join::WITH, 'g.id = p.gameId')
            ->andWhere('p.userId = :userId')
            ->setParameter('userId', $id->getBytes());

        if(null !== $state){
            $qb
                ->andWhere('p.state = :state')
                ->setParameter('state', $state);

        }

        return $qb->getQuery()->getResult();
    }

    public function getGamesClaimingById(UuidInterface $id): array
    {
        return $this->getGamesByParticipant($id, ParticipantState::claimer()->getRawValue());
    }

    public function getGamesPlayingById(UuidInterface $id): array
    {
        return $this->getGamesByParticipant($id, ParticipantState::player()->getRawValue());
    }

    public function getGamesOpenToUserById(UuidInterface $id): array
    {
        $qb = $this->createQueryBuilder('g');
        $qb
            ->leftJoin(Participant::class, 'p', Join::WITH, 'g.id = p.gameId')
            ->andWhere('p.userId <> :userId')
            ->andWhere('g.masterId <> :userId')
            ->andWhere('g.status = :statusOpen')
            ->setParameter('userId', $id->getBytes())
            ->setParameter('statusOpen', Game::STATE_OPEN_GAME);

        return $qb->getQuery()->getResult();
    }
}