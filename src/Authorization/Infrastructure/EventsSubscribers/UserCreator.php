<?php

declare(strict_types=1);

namespace App\Authorization\Infrastructure\EventsSubscribers;

use App\Authorization\Application\UserWriteStorage;
use App\Authorization\Domain\Events\AccountRegistered;
use App\Authorization\Infrastructure\Entity\User;
use App\Common\Application\DomainEvent;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

final class UserCreator
{
    private UserPasswordEncoderInterface $encoder;

    private UserWriteStorage $userWriteStorage;

    /**
     * @param UserPasswordEncoderInterface $encoder
     * @param UserWriteStorage $userWriteStorage
     */
    public function __construct(UserPasswordEncoderInterface $encoder, UserWriteStorage $userWriteStorage)
    {
        $this->encoder = $encoder;
        $this->userWriteStorage = $userWriteStorage;
    }

    /**
     * @param DomainEvent|AccountRegistered $message
     */
    public function notify(DomainEvent $message)
    {
        $user = User::createNew($message->getUsername());
        $user->setId($message->getUserId());
        $user->setPassword($this->encoder->encodePassword($user, $message->getPassword()));

        $this->userWriteStorage->add($user);
    }
}