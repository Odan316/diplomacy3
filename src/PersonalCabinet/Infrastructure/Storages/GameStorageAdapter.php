<?php

declare(strict_types=1);

namespace App\PersonalCabinet\Infrastructure\Storages;

use App\Game\Domain\Game as OuterGame;
use App\PersonalCabinet\Application\Game;
use App\Game\Application\GameReadStorage as GameStorageOuter;
use App\PersonalCabinet\Application\GameReadStorage;
use Ramsey\Uuid\UuidInterface;

final class GameStorageAdapter implements GameReadStorage
{
    private GameStorageOuter $outerStorage;

    public function __construct(GameStorageOuter $outerStorage)
    {
        $this->outerStorage = $outerStorage;
    }

    public function getGamesByMasterId(UuidInterface $id): array
    {
        $outerGames = $this->outerStorage->getGamesByMasterId($id);

        return $this->convertGames($outerGames);
    }

    public function getGamesClaimingById(UuidInterface $id): array
    {
        return $this->convertGames(
            $this->outerStorage->getGamesClaimingById($id)
        );
    }

    public function getGamesPlayingById(UuidInterface $id): array
    {
        return $this->convertGames(
            $this->outerStorage->getGamesPlayingById($id)
        );
    }

    public function getGamesOpenToUserById(UuidInterface $id): array
    {
        return $this->convertGames(
            $this->outerStorage->getGamesOpenToUserById($id)
        );
    }

    /**
     * @param OuterGame[] $outerGames
     * @return Game[]
     */
    private function convertGames(array $outerGames)
    {
        $games = [];
        foreach($outerGames as $outerGame){
            $games[] = new Game($outerGame->getTitle(), $outerGame->getTag());
        }

        return $games;
    }
}