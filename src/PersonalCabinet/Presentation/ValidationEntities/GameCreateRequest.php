<?php

namespace App\PersonalCabinet\Presentation\ValidationEntities;

use Symfony\Component\Validator\Constraints as Assert;

class GameCreateRequest
{
    /**
     * @Assert\NotBlank()
     * @Assert\Length(min="3", max="12")
     */
    public string $title;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min="3", max="3")
     */
    public string $moduleTag;
}