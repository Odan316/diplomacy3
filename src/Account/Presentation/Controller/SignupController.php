<?php

declare(strict_types=1);

namespace App\Account\Presentation\Controller;

use App\Account\Application\SignupProcessor;
use App\Account\Presentation\Form\SignupForm;
use App\Account\Presentation\ValidationEntities\SignupRequest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SignupController extends AbstractController
{
    /**
     * @param Request $request
     * @param SignupProcessor $signupProcessor
     * @return Response
     */
    public function signup(Request $request, SignupProcessor $signupProcessor): Response
    {
        $signupRequest = new SignupRequest();
        $signUpForm = $this->createForm(SignupForm::class, $signupRequest);

        $signUpForm->handleRequest($request);
        if ($signUpForm->isSubmitted() && $signUpForm->isValid()) {
            $signupProcessor->registerAccount($signupRequest);

            return $this->redirectToRoute('app_login');
        }

        return $this->render('default/signup.html.twig', [
            'signUpForm' => $signUpForm->createView(),
        ]);
    }
}