<?php

declare(strict_types=1);

namespace App\PersonalCabinet\Application;

final class Game
{
    private string $title;

    private string $tag;

    public function __construct(string $title, string $tag)
    {
        $this->title = $title;
        $this->tag = $tag;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getTag(): string
    {
        return $this->tag;
    }
}