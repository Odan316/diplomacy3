<?php

declare(strict_types=1);

namespace App\Authorization\Infrastructure\Entity;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class User implements UserInterface
{
    private UuidInterface $id;

    private string $password;

    private array $roles = [];

    private string $username;

    public static function createNew(string $username, array $roles = []): User
    {
        $user = new self;

        $user->setId(Uuid::uuid4());
        $user->setUsername($username);
        $user->setRoles($roles);

        return $user;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function setId(UuidInterface $id)
    {
        $this->id = $id;
    }

    public function getRoles(): array
    {
        $roles = $this->roles;
        if(0 === count($roles)){
            $roles[] = 'ROLE_USER';
        }

        return array_unique($roles);
    }

    public function setRoles(array $roles): void
    {
        $this->roles = $roles;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password)
    {
        $this->password = $password;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setUsername(string $username)
    {
        $this->username = $username;
    }

    public function getSalt(): ?string
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
        return null;
    }

    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }
}